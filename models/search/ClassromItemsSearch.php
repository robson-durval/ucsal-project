<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassromItems;

/**
 * ClassromItemsSearch represents the model behind the search form of `app\models\ClassromItems`.
 */
class ClassromItemsSearch extends ClassromItems
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'classrom_id', 'item_id'], 'integer'],
            [['id', 'classrom_id', 'item_id', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClassromItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // var_dump($params);
        // die();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'classrom_id' => $this->classrom_id,
            'item_id' => $this->item_id,
        ]);

        

        return $dataProvider;
    }
}
