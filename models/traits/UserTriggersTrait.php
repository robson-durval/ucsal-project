<?php

namespace app\models\traits;


use app\models\Chat;

trait UserTriggersTrait
{

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        // in this case, we will use the old hashed password.
        if(empty($this->password) && empty($this->password_compare) && !empty($this->initialPassword))
            $this->password=$this->password_compare=$this->initialPassword;

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        parent::afterFind();

        //reset the password to null because we don't want the hash to be shown.
        $this->initialPassword = $this->password;

       // $this->fullName = $this->getFullName();

        if(in_array($this->scenario, ['register','changePassword'])) {
            $this->password = null;
        }
    }

}