<?php

namespace app\models\traits;

use app\models\Sectors;
use app\models\Users;
use Yii;

trait UserHelpersTrait
{
    /**
     * Retorna a url do avatar
     * @return string
     */
    public function getAvatarUrl()
    {
        if (!$this->isNewRecord && trim($this->avatar) != "" && file_exists(Yii::getAlias('@app/web/files/images/users').'/'.$this->avatar)) {
            return Yii::$app->request->BaseUrl.'/files/images/users/'.$this->avatar;
        }

        return Yii::$app->request->BaseUrl.'/files/images/blank-avatar.jpg';
    }

    /**
     * Retorna a url do avatar
     * @return string
     */
    public function getUserUrlPhoto($avatar)
    {
        if (trim($avatar) != "" && file_exists(Yii::getAlias('@app/web/files/images/users').'/'.$avatar)) {
            return Yii::$app->request->BaseUrl.'/files/images/users/'.$avatar;
        }

        return Yii::$app->request->BaseUrl.'/files/images/blank-avatar.jpg';
    }

    /**
     * @param string $avatar
     * @return string
     */
    public static function staticAvatarUrl($avatar)
    {
        if (trim($avatar) != "" && file_exists(Yii::getAlias('@app/web/files/images/users').'/'.$avatar)) {
            return Yii::$app->request->BaseUrl.'/files/images/users/'.$avatar;
        }

        return Yii::$app->request->BaseUrl.'/files/images/blank-avatar.jpg';
    }

    /**
     * @param string $sectorName
     * @return array
     */
    public static function getListNameAndFunctionBySector($sectorName) {

        $response = [];
        if(($sector = Sectors::findOne(['name' => $sectorName])) != null) {

            if(($users = Users::findAll(['sector_id' => $sector->id])) != null) {

                foreach($users as $user) {
                    $response[$user->id] = $user->first_name . ' (Fn: '.$user->function->name.')';
                }
            }
        }

        return $response;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @return string
     */
    public function getNameAndFunction()
    {
        return $this->first_name.' ('.$this->function->name.')';
    }

    /**
     * @return string
     */
    public function getNameWithFunctionAndSector()
    {
        $function = $this->function;

        return $this->first_name.' ('.$function->name.' - '.$function->sector->name.')';
    }

    /**
     * @return string
     */
    public function getFullNameWithFunctionAndSector()
    {
        $function = $this->function;

        return $this->getFullName().' ('.$function->name.' - '.$function->sector->name.')';
    }

    /**
     * @param bool $array
     * @return array
     */
    public function getSameSectorUsers($array=false)
    {
        $functions = $this->function->sector->functions;
        $response = [];

        if (sizeof($functions)) {

            foreach ($functions  as $function) {

                $users = $function->getUsers()->asArray($array)->all();

                $response = array_merge($response, $users);
            }
        }

        return $response;
    }

    /**
     * @param string $sectorName
     * @param bool $groupFunction
     * @param bool|array $notId
     * @param bool|string $onlyFunction
     * @return array
     */
    public static function getListUsersBySectorWithGroup($sectorName, $groupFunction=false, $notId=[], $onlyFunction=false,$branche = false, $ativo = false)
    {
        $response = [];
        $sector = Sectors::findOne(['name' => trim($sectorName)]);

        if (!$sector) {
            return $response;
        }


        if($branche){

            $query = $sector->getUsers()->where(['branches_id'=>$branche, 'status' => 1])->orderBy('first_name');
        }else{
            $query = $sector->getUsers()->where(['status'=>1])->orderBy('first_name');
        }


        if($ativo){
            $query->where(['status'=>1]);
        }     


        if (sizeof($notId)) {
            $query->where(['not in','id',$notId]);
        }

        $users = $query->all();

        if (sizeof($users)) {
            foreach ($users as $user) {

                if ($groupFunction && !$onlyFunction) {
                    $response[$user->id] = $user->fullName;
                } else {
                    $function = $user->function->name;

                    if ($onlyFunction) {

                        if (trim($function) == $onlyFunction) {
                            $response[$user->id] = $user->fullName;
                        }
                    } else {
                        $response[$function][$user->id] = $user->fullName;
                    }
                }
            }
        }
        //ddd($response);
        return $response;
    }

    /**
     * Total de condomínios operacionais que é responsável
     * @return int
     */
    public function getTotalHasCondominiums()
    {
        return $this->getCondominiums()->count();
    }

    /**
     * Retorna a ultima visita operacional feita pelo consultor
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getLastOperationalVisit()
    {
        return $this->getOperationalVisits()->where('date <= now()')->orderBy(['date' => SORT_DESC])->one();
    }

    /**
     * @param string $name
     * @return Users|null
     */
    public static function getByName($name)
    {
        return self::find()->where("first_name LIKE \"%{$name}%\" OR last_name LIKE \"%{$name}%\"")->one();
    }
}