<?php

namespace app\models;

use Yii;
use app\models\Classroms;

/**
 * This is the model class for table "classes".
 *
 * @property int $id
 * @property int $classrom_id
 * @property string $date
 * @property string $matter
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Classroms $classrom
 */
class Classes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['classrom_id', 'date'], 'required'],
            [['classrom_id'], 'integer'],
            [['date', 'created_at', 'updated_at', 'matter'], 'safe'],
            [['classrom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Classroms::className(), 'targetAttribute' => ['classrom_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'classrom_id' => 'Sala de Aula Nº',
            'date' => 'Data',
            'matter' => 'Matéria',
            'created_at' => 'Cadastrado em',
            'updated_at' => 'Última atualização feita em',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassrom()
    {
        return $this->hasOne(Classroms::className(), ['id' => 'classrom_id']);
    }
}
