<?php

namespace app\models;

use Yii;
use app\models\Classroms;
use app\models\Items;

/**
 * This is the model class for table "classrom_items".
 *
 * @property int $id
 * @property int $classrom_id
 * @property int $item_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Classroms $classrom
 * @property Items $item
 */
class ClassromItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classrom_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['classrom_id', 'item_id'], 'required'],
            [['classrom_id', 'item_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            // [['classrom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Classroms::className(), 'targetAttribute' => ['classrom_id' => 'id']],
            // [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'classrom_id' => 'Classe Nº',
            'item_id' => 'Item',
            'created_at' => 'Cadastrado em',
            'updated_at' => 'Última atualização feita em',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassrom()
    {
        return $this->hasOne(Classroms::className(), ['id' => 'classrom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'item_id']);
    }
}
