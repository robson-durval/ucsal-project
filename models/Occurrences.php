<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "occurrences".
 *
 * @property int $id
 * @property int $created_by
 * @property int $item_id
 * @property int $classrom_id
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $createdBy
 * @property Items $item
 * @property Classroms $classrom
 */
class Occurrences extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'occurrences';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'item_id', 'classrom_id', 'text'], 'required'],
            [['created_by', 'item_id', 'classrom_id'], 'integer'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['classrom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Classroms::className(), 'targetAttribute' => ['classrom_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Criado por',
            'item_id' => 'Item',
            'classrom_id' => 'Sala de Aula',
            'text' => 'Descrição',
            'created_at' => 'cadastrado em',
            'updated_at' => 'Última atualização feita em',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassrom()
    {
        return $this->hasOne(Classroms::className(), ['id' => 'classrom_id']);
    }
}
