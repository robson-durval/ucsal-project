<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Classes;

/**
 * This is the model class for table "classes_users".
 *
 * @property int $id
 * @property int $user_id
 * @property int $classe_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Classes $classe
 * @property Users $user
 */
class ClassesUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classes_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'classe_id'], 'required'],
            [['user_id', 'classe_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['classe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Classes::className(), 'targetAttribute' => ['classe_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Usuário',
            'classe_id' => 'Classe',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização feita em',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasse()
    {
        return $this->hasOne(Classes::className(), ['id' => 'classe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
