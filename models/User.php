<?php

namespace app\models;

use app\components\BaseModel;
use app\models\traits\UserHelpersTrait;
use app\models\traits\UserIdentityTrait;
use app\models\traits\UserTriggersTrait;

class User extends BaseModel implements \yii\web\IdentityInterface
{
    public $password_compare;
    public $initialPassword;

    const USER_PROFILE_STUDENT = 1;
    const USER_PROFILE_TEACHER = 2;
    const USER_PROFILE_COORDINATOR = 3;

    public static $profiles = [
        self::USER_PROFILE_STUDENT => 'Aluno',
        self::USER_PROFILE_TEACHER => 'Professor',
        self::USER_PROFILE_COORDINATOR => 'Coordenador'
    ];

    /**
     * Traits
     */
    use UserIdentityTrait;
    use UserHelpersTrait;
    use UserTriggersTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password', 'profile'], 'required'],
            [['name', 'username', 'email', 'password'], 'string', 'max' => 255],
            [['profile'], 'integer',],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nome',
            'email' => 'E-mail',
            'password' => 'Senha',
            'profile' => 'Perfil',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização',
        ];
    }
}
