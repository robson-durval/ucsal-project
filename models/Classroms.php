<?php

namespace app\models;

use Yii;
use app\models\Modules;

/**
 * This is the model class for table "classroms".
 *
 * @property int $id
 * @property int $module_id
 * @property int $number
 * @property string $location
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Modules $module
 */
class Classroms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classroms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'number', 'location'], 'required'],
            [['module_id', 'number'], 'integer'],
            [['location'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Módulo',
            'number' => 'Número',
            'location' => 'Localização',
            'created_at' => 'Criado em',
            'updated_at' => 'Última atualização feita em',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }
}
