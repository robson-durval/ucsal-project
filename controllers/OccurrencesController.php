<?php

namespace app\controllers;

use Yii;
use app\models\Occurrences;
use app\models\search\OccurrencesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ClassromItems;
use app\models\Items;
use yii\helpers\ArrayHelper;

/**
 * OccurrencesController implements the CRUD actions for Occurrences model.
 */
class OccurrencesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Occurrences models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OccurrencesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Occurrences model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Occurrences model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->profile != 3) {
            $model = new Occurrences();

            if ($model->load(Yii::$app->request->post())) {
                $model->created_by = Yii::$app->user->id;
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['index']);
        }

    }

    /**
     * Updates an existing Occurrences model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->profile != 3) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['index']);
        }
        
    }

    /**
     * Deletes an existing Occurrences model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetItemsByClasse($classe_id)
    {
        $classrom = ClassromItems::find()->where(['classrom_id' => $classe_id])->all();
        $ids = [];
        foreach ($classrom as $key => $value) {
            $ids[] = $value->item_id;
        }
        
        $items = Items::find()->where(['in', 'id', $ids])->all();

        $html = '';

        foreach ($items as $key => $value) {
            $html .= '<option value="'.$value->id.'"> '.$value->name.' </option>';
        }

        return $html;
    }

    /**
     * Finds the Occurrences model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Occurrences the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Occurrences::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
