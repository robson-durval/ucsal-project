<?php
/**
 * Created by PhpStorm.
 * User: edjo
 * Date: 20/11/15
 * Time: 09:02
 */

namespace app\components;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


abstract class BaseModel extends \yii\db\ActiveRecord
{
    /*Array com ids de setores que podem ver tudo em todas as listas*/
    public static $ARRAY_SEE_ALL_REGISTER_SECTORS= array('11','14', '1');

    /*
     * Array com Ids de perfis que podem ver tudo em todas as listas
     */
    public static $ARRAY_SEE_ALL_REGISTER_PERFIS= array('4','48','18', '81', '44');

    /*
     * Arrays usados para verificar os acessos nas listas
     */
    //Array de supervisores que necessitam de aprovação da gerência
        public static $ARRAY_SUPERVISORS_MANAGER = array('42','59','33','23','38','44','59','68','81');
    //Array de supervisores que necessitam de aprovação apenas da diretoria
        public static $ARRAY_SUPERVISORS_DIRECTOR = array('4','48','54','64','9','35');
    //Array de perfis de gerencia
        public static $ARRAY_MANAGER = array('159','160','161');
    //Array de perfis de Diretor
        public static $ARRAY_DIRECTOR = array('18');


    /*
     * Arrays COM IDS DOS SETORES DE RESPONSABILIDADE DA GERENCIA
     */
        public static $ARRAY_SECTORS_MANAGER = array('2','4','7','9','12','13','16','17','20');



    public  static  $BANKS = [ 'BRADESCO' => 'BRADESCO', 'CAIXA' => 'CAIXA', 'BANCO DO BRASIL' => 'BANCO DO BRASIL', 'SANTANDER' => 'SANTANDER', 'ITAÚ' => 'ITAÚ', ];




    /**
     * @var array
     */
    public static $MONTH= [
        'JANEIRO'=>'JANEIRO',
        'FEVEREIRO'=>'FEVEREIRO',
        'MARÇO'=>'MARÇO',
        'ABRIL'=>'ABRIL',
        'MAIO'=>'MAIO',
        'JUNHO'=>'JUNHO',
        'JULHO'=>'JULHO',
        'AGOSTO'=>'AGOSTO',
        'SETEMBRO'=>'SETEMBRO',
        'OUTUBRO'=>'OUTUBRO',
        'NOVEMBRO'=>'NOVEMBRO',
        'DEZEMBRO'=>'DEZEMBRO',
    ];

    /**
     * @var array
     */
    public static $MONTH2= [
        '1'=>'Janeiro',
        '2'=>'Fevereiro',
        '3'=>'Março',
        '4'=>'Abril',
        '5'=>'Maio',
        '6'=>'Junho',
        '7'=>'Julho',
        '8'=>'Agosto',
        '9'=>'Setembro',
        '10'=>'Outubro',
        '11'=>'Novembro',
        '12'=>'Dezembro',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behavior = parent::behaviors();

        if($this->hasAttribute('created_at')) {

            $behavior['timestamp'] = [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ];
        }

        return $behavior;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function date($attribute, $params)
    {
        if ($this->{$attribute} != "") {
            $this->{$attribute} = \app\components\Helper::convertStringToDatabaseDate($this->{$attribute});
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function decimal($attribute, $params)
    {
        if ($this->{$attribute} != "") {
            $this->{$attribute} = str_replace(',', '.', str_replace('.', '', $this->{$attribute}));
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateUsername($attribute, $params)
    {

        if (preg_match('/[^a-z])/i', $this->$attribute)) {
            $this->addError($attribute, 'Username should only contain alphabets');

        }
        if ( ! preg_match('/^.{3,8}$/', $this->$attribute) ) {
            $this->addError($attribute, 'Username must be bwtween 3 to 8 characters.');
        }
    }

    /**
     * Users::getListForDropDown('id', 'first_name', true, 'fullName')
     * @param string $pk
     * @param string $label
     * @param bool $fakeLabel
     * @param bool $orderBy
     * @return array
     */
    public static function getListForDropDown($pk, $label, $orderBy=true, $fakeLabel=false)
    {
        $data = self::find();

        if ($orderBy) {
            $data->orderBy($label);
        }

        return ArrayHelper::map($data->all(), $pk, $fakeLabel?$fakeLabel:$label);
    }

    public function getFieldBoolLabel($field)
    {
        if ($this->{$field} == null)
            return null;

        return $this->{$field} ? '<span class="label label-green">Sim</span>' : '<span class="label label-red">Não</span>';
    }

    public function parseDateFormat($attribute, $format='d/m/Y')
    {
        $result = '';
        if($this->hasAttribute($attribute) && $this->$attribute) {

            $result = date($format, strtotime($this->$attribute));
        }

        return $result;
    }

}