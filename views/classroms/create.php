<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Classroms */

$this->title = 'Cadastrar Sala de Aula';
$this->params['breadcrumbs'][] = ['label' => 'Salas de Aula', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classroms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
