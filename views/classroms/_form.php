<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Modules;

/* @var $this yii\web\View */
/* @var $model app\models\Classroms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classroms-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'module_id')->dropDownList(ArrayHelper::map(Modules::find()->all(),'id', 'name')) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true, 'type' => 'number']) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
