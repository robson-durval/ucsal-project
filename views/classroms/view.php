<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Classroms */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Salas de Aulas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classroms-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja remover esta sala de aula?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'module_id',
                'type' => 'html',
                'value' => $model->module->name,
            ],
            'number',
            'location:ntext',
            [
                'attribute' => 'created_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
            [
                'attribute' => 'updated_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
        ],
    ]) ?>

</div>
