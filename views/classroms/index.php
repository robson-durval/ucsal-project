<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Modules;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClassromsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Salas de Aulas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classroms-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastrar Sala de Aula', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'module_id',
                'filter' => ArrayHelper::map(Modules::find()->all(), 'id', 'name'),
                'value' => function($model) {
                    return $model->module->name;
                }
            ],
            'number',
            'location:ntext',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
