<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Remover', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja remover essa classe?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'classrom_id',
                'type' => 'html',
                'value' => $model->classrom->number,
            ],
            [
                'attribute' => 'date',
                'type' => 'html',
                'value' => date("d/m/Y"),
            ],
            [
                'attribute' => 'matter',
                'type' => 'html',
                'value' => $model->matter,
            ],
            [
                'attribute' => 'created_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
            [
                'attribute' => 'updated_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
        ],
    ]) ?>

</div>
