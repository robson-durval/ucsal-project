<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Classroms;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    	<div class="col-sm-3">
    	    <?= $form->field($model, 'classrom_id')->dropDownList(ArrayHelper::map(Classroms::find()->all(),'id', 'number'), ['prompt' => 'Selecione...']) ?>
    	</div>

    	<div class="col-sm-3">
            <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'type' => 'date']) ?>
        </div>

        <div class="col-sm-3">
    	    <?= $form->field($model, 'matter')->textInput(['maxlength' => true]) ?>
    	</div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
