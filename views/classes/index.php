<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClassesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Classes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastrar Classe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'classrom_id',
                'value' => function ($model) {
                    return $model->classrom->number;
                }
            ],

            [
                'attribute' => 'matter',
                'value' => function ($model) {
                    return $model->matter;
                }
            ],

            [
                'attribute' => 'date',
                'value' => function ($model) {
                    return date("d/m/Y", strtotime($model->date));
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
