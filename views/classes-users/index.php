<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Classes;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClassesUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuários e Classes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Vincular usuário a uma classe', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'filter' => ArrayHelper::map(User::find()->where('profile = 1 or profile = 2')->all(), 'id', 'name'),
                'value' => function ($model) {
                    return $model->user->name;
                }
            ],

            [
                'attribute' => 'classe_id',
                'filter' => ArrayHelper::map(Classes::find()->all(), 'id', function ($model) {
                    return 'Sala Nº '. $model->classrom->number . ' | dia ' . date("d/m/Y", strtotime($model->date));
                }),
                'value' => function ($model) {
                    return 'Sala Nº '. $model->classe->classrom->number . ' | dia ' . date("d/m/Y", strtotime($model->classe->date));
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
