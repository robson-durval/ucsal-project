<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClassesUsers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Classe e Usuário', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Remover', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => $model->user->name,
            ],
            [
                'attribute' => 'classe_id',
                'value' => 'Sala Nº '. $model->classe->classrom->number . ' | dia ' . date("d/m/Y", strtotime($model->classe->date)),
            ],
            [
                'attribute' => 'created_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
            [
                'attribute' => 'updated_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
        ],
    ]) ?>

</div>
