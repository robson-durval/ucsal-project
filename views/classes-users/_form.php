<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Classes;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ClassesUsers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    	<div class="col-sm-3">
    	    <?= $form->field($model, 'classe_id')->dropDownList(ArrayHelper::map(Classes::find()->all(), 'id', function ($model) {
                    return 'Sala Nº '. $model->classrom->number . ' | dia ' . date("d/m/Y", strtotime($model->date));
                }), ['prompt' => 'Selecione...']) ?>
    	</div>

    	<div class="col-sm-3">
    	    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->where('profile = 1 or profile = 2')->all(), 'id', function ($model) {
                    return $model->name . ' (' .$model::$profiles[$model->profile].')';
                }), ['prompt' => 'Selecione...']) ?>
    	</div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
