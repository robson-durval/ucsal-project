<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClassesUsers */

$this->title = 'Vincular usuário a uma classe';
$this->params['breadcrumbs'][] = ['label' => 'Classes Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-users-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
