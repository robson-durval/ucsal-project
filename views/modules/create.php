<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Modules */

$this->title = 'Cadastrar Módulo';
$this->params['breadcrumbs'][] = ['label' => 'Módulos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
