<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Projeto Ruy BarbosaL',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Classes', 'url' => ['/classes/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Itens', 'url' => ['/items/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Módulos', 'url' => ['/modules/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Salas de aula', 'url' => ['/classroms/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Itens de sala de aula', 'url' => ['/classrom-items/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Usuários em classes', 'url' => ['/classes-users/index']] : '',
            !Yii::$app->user->isGuest && Yii::$app->user->identity->profile == 3 ? ['label' => 'Usuários', 'url' => ['/user/index']] : '',

            !Yii::$app->user->isGuest ? ['label' => 'Ocorrências', 'url' => ['/occurrences/index']] : '',
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ruy Barbosa <?= date('Y') ?></p>

        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
