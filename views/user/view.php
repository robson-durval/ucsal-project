<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja deletar este usuário?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'username',
            'email:email',
            [
                'attribute' => 'profile',
                'type' => 'html',
                'value' => $model::$profiles[$model->profile],
            ],
            [
                'attribute' => 'created_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
            [
                'attribute' => 'updated_at',
                'type' => 'html',
                'value' => date("d/m/Y H:i:s"),
            ],
        ],
    ]) ?>

</div>
