<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Items;
use app\models\Classroms;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ClassromItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Itens de sala de aula';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classrom-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Vincular item a sala de aula', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'classrom_id',
                'filter' => ArrayHelper::map(Classroms::find()->all(), 'id', 'number'),
                'value' => function ($model) {
                    return $model->classrom->number;
                }
            ],

            [
                'attribute' => 'item_id',
                'filter' => ArrayHelper::map(Items::find()->all(), 'id', 'name'),
                'value' => function ($model) {
                    return $model->item->name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
