<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClassromItems */

$this->title = 'Editar item da classe: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Itens da Classe', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="classrom-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
