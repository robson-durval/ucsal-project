<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Classroms;
use app\models\Items;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ClassromItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classrom-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    	<div class="col-sm-3">
    	    <?= $form->field($model, 'classrom_id')->dropDownList(ArrayHelper::map(Classroms::find()->all(),'id', 'number'), ['prompt' => 'Selecione...']) ?>
    	</div>

    	<div class="col-sm-3">
    	    <?= $form->field($model, 'item_id')->dropDownList(ArrayHelper::map(Items::find()->all(),'id', 'name'), ['prompt' => 'Selecione...']) ?>
    	</div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
