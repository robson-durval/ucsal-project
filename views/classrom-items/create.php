<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClassromItems */

$this->title = 'Vincular Item a Sala de Aula';
$this->params['breadcrumbs'][] = ['label' => 'Itens de sala de aula', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classrom-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
