<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Occurrences */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Occurrences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occurrences-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_by',
            'item_id',
            'classrom_id',
            'text:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
