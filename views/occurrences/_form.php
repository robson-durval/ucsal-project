<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Classroms;
use app\models\Items;
use app\models\ClassesUsers;
use app\models\Classes;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Occurrences */
/* @var $form yii\widgets\ActiveForm */

$classes_array = ClassesUsers::find()->select('classe_id')->where(['user_id' => Yii::$app->user->id])->asArray()->all();

$ids = [];

foreach ($classes_array as $key => $value) {
    $ids[] = $value['classe_id'];
}

$classes = Classes::find()->select('classrom_id')->where(['in', 'id', $ids])->asArray()->all();

$ids = [];

foreach ($classes as $key => $value) {
    $ids[] = $value['classrom_id'];
}

?>

<div class="occurrences-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'classrom_id')->dropDownList(ArrayHelper::map(Classroms::find()->where(['in', 'id', $ids])->all(), 'id', 'number'), ['prompt' => 'Selecione...']) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'item_id')->dropDownList([], ['prompt' => 'Selecione...']) ?>
        </div>

        <div class="col-sm-6">
             <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS

$(document).ready(function() {
    $(document).on('change', '#occurrences-classrom_id', function(){
        if( $(this).val() != "" ) {
            $.ajax({
                method: "GET",
                url: "get-items-by-classe?classe_id="+$("#occurrences-classrom_id").val(),
            }).done(function(data){
                $("#occurrences-item_id").html("");
                $("#occurrences-item_id").append(data);
            });
        }else{
            $("#occurrences-item_id").html("");
        }
    });
});

JS;
$this->registerJs($script, View::POS_END);
?>