<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Occurrences */

$this->title = 'Cadastrar Ocorrência';
$this->params['breadcrumbs'][] = ['label' => 'Ocorrências', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occurrences-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
