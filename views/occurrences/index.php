<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Items;
use app\models\Classroms;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OccurrencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ocorrências';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="occurrences-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(!Yii::$app->user->isGuest  != null && (Yii::$app->user->identity->profile == 1 || Yii::$app->user->identity->profile == 2)) { ?>
    <p>
        <?= Html::a('Cadastrar Ocorrência', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'created_by',
                'filter' => ArrayHelper::map(User::find()->where('profile = 1 or profile = 2')->all(), 'id', 'name'),
                'value' => function ($model) {
                    return $model->createdBy->name .' ('.$model->createdBy::$profiles[$model->createdBy->profile].')';
                }
            ],
            [
                'attribute' => 'item_id',
                'filter' => ArrayHelper::map(Items::find()->all(), 'id', 'name'),
                'value' => function ($model) {
                    return $model->item->name;
                }
            ],
            [
                'attribute' => 'classrom_id',
                'filter' => ArrayHelper::map(Classroms::find()->all(), 'id', 'number'),
                'value' => function ($model) {
                    return $model->classrom->number;
                }
            ],
            'text:ntext',
            //'created_at',
            //'updated_at',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{view} {update} {delete}",
                'buttons' => [
                    
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'Visualizar']);
                    },
                    'update' => function ($url, $model) {
                        if ($model->created_by == Yii::$app->user->id) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => 'Editar']);
                        }
                    },
                    
                    'delete' => function ($url, $model) {
                        if ($model->created_by == Yii::$app->user->id) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => 'Deletar',
                                'data-confirm' => 'Tem certeza de que deseja excluir este item?',
                                'data-method' => 'post',
                            ]);
                        }
                    }
                ]
            ],
        ],
    ]); ?>
</div>
